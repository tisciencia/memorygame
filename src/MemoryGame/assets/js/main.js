
(function($) {
		
	var MemoryGame = {
		init: function() {
			MemoryGame.resetGame();
			
			this.bindEvents();

			return this;
		},

		resetGame: function() {
			this.HITS = 6;

			MemoryGame.lastMove = [];
			MemoryGame.cards = [];
			MemoryGame.movementsCount = 0;
			MemoryGame.hitsCount = 0;

			this.setData();
			this.startGame();
			
		},

		bindEvents: function() {
			$('.flip').live( 'click', this.moveCard );
		},

		moveCard: function() {
			var card = $(this);
			if(!card.hasClass('show')){ 
				MemoryGame.increaseMovementsCount(1);
				MemoryGame.show(card);
				MemoryGame.tryMacth(card);
			}
		},

		increaseMovementsCount: function(value) {
			MemoryGame.movementsCount += value;
			$('#move-count').html(MemoryGame.movementsCount);
		},

		tryMacth: function(card) {
			if(MemoryGame.lastMove.length > 0){
				var lastMoveCard = MemoryGame.lastMove.pop();
				if (MemoryGame.isMacth(lastMoveCard, card)){
					MemoryGame.hitsCount += 1;
					if(MemoryGame.HITS == MemoryGame.hitsCount){
						alert('you win');
						MemoryGame.resetGame();
						MemoryGame.hideAll();
					}
				}else{
					setTimeout( function() {
						MemoryGame.hide(card);
						MemoryGame.hide(lastMoveCard)	
					}, 1000);
				}
			}else{
				MemoryGame.lastMove.push(card);
			}
		},

		isMacth: function(lastCard, actualCard)
		{
			idCard1 = lastCard.attr('id').split('-')[1];
			idCard2 = actualCard.attr('id').split('-')[1];
			return idCard1 == idCard2;
		},

		show: function(card) {
			card.addClass('show');
		},

		hide: function(card) {
			card.removeClass('show');
		},

		showAll: function() {
			$('.flip').addClass('show');
		},

		hideAll: function() {
			$('.flip').removeClass('show');	
		},

		setData: function() {
			var cardsData = Array();
			cardsData[0] = "card-1-1;../assets/img/apple_1.png";
			cardsData[1] = "card-2-1;../assets/img/apple_2.png";
			cardsData[2] = "card-3-1;../assets/img/apple_3.png";
			cardsData[3] = "card-4-1;../assets/img/apple_4.png";
			cardsData[4] = "card-5-1;../assets/img/apple_5.png";
			cardsData[5] = "card-6-1;../assets/img/apple_6.png";
			cardsData[6] = "card-7-1;../assets/img/apple_7.png";
			cardsData[7] = "card-8-1;../assets/img/apple_8.png";
			cardsData[8] = "card-1-2;../assets/img/apple_1.png";
			cardsData[9] = "card-2-2;../assets/img/apple_2.png";
			cardsData[10] = "card-3-2;../assets/img/apple_3.png";
			cardsData[11] = "card-4-2;../assets/img/apple_4.png";
			cardsData[12] = "card-5-2;../assets/img/apple_5.png";
			cardsData[13] = "card-6-2;../assets/img/apple_6.png";
			cardsData[14] = "card-7-2;../assets/img/apple_7.png";
			cardsData[15] = "card-8-2;../assets/img/apple_8.png";

			cardsData.sort(MemoryGame.randomOrder);
			
			var cards = [];
			for (i=0; i<cardsData.length; i++) {
				var data = cardsData[i].split(";");
				var card = {
        			'Id': data[0],
        			'Image': data[1]
   	 			}
   	 			cards.push(card);
			}

			MemoryGame.cards = cards;
		},

		startGame: function() {
			$("#container").html("");
			$( "#cardTemplate" ).tmpl( MemoryGame.cards ).appendTo( "#container" );
			
			setTimeout( function() {
				MemoryGame.showAll();	
				setTimeout( function() {
					MemoryGame.hideAll();	
				}, 3000);
			}, 1000);
			
		},
		
		randomOrder: function() {
			return (Math.round(Math.random())-0.5);
		}
	};

	window.MemoryGame = MemoryGame.init();

})(jQuery);
